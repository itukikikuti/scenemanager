#pragma once
#include <memory>

class Scene
{
public:
    virtual void Update() = 0;
};

class SceneManager
{
public:
    template<class T = Scene> static void Load()
    {
        scene = std::make_unique<T>();
    }

    static void Update()
    {
        scene->Update();
    }

private:
    static std::unique_ptr<Scene> scene;
};
