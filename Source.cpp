#include "XLibrary11.hpp"
#include "SceneManager.h"

class Title;
class Game;
class Result;

class Title : public Scene
{
public:
    void Update() override
    {
        printf("Title\n");

        if (XLibrary::Input::GetKeyDown(VK_RETURN))
        {
            SceneManager::Load<Game>();
        }
    }
};

class Game : public Scene
{
public:
    void Update() override
    {
        printf("Game\n");

        if (XLibrary::Input::GetKeyDown(VK_RETURN))
        {
            SceneManager::Load<Result>();
        }
    }
};

class Result : public Scene
{
public:
    void Update() override
    {
        printf("Result\n");

        if (XLibrary::Input::GetKeyDown(VK_RETURN))
        {
            SceneManager::Load<Title>();
        }
    }
};

int main()
{
    SceneManager::Load<Title>();

    while (XLibrary::Refresh())
    {
        SceneManager::Update();
    }

    return 0;
}
